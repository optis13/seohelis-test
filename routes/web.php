<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', function () {
    return redirect('/feeds');
});

Route::get('/feeds', 'FeedsController@index');
Route::get('/feeds/{categorieid}', ['uses' => 'FeedsController@feeds']);

Route::get('/admin/categories', 'Admin\CategoriesController@index');
Route::get('/admin/categories/{categoryid}', 'Admin\CategoriesController@index');
Route::post('/admin/categories/{categoryid}', 'Admin\CategoriesController@crud');
Route::post('/admin/categories', 'Admin\CategoriesController@crud');

Route::get('/admin/feeds', 'Admin\FeedsController@index');
Route::get('/admin/feeds/{feedid}', 'Admin\FeedsController@index');
Route::post('/admin/feeds/{feedid}', 'Admin\FeedsController@crud');
Route::post('/admin/feeds', 'Admin\FeedsController@crud');

Route::get('/home', 'FeedsController@index');
Route::get('/changepassword', 'Auth\ChangePasswordController@index');
Route::post('/changepassword', 'Auth\ChangePasswordController@changepassword');

Auth::routes();

