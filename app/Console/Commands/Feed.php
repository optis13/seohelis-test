<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Feed extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Feed update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('Starting...');
        $feedurls = \App\Feedsurls::all();
        DB::table('feeds')->truncate();
        foreach ($feedurls as $value) {
            $this->info('updating: ' . $value->feedurl);
            $xml = new \SimpleXMLElement($value->feedurl, 0, true);
            $this->updateFeed($xml, $value->id);
            $this->info('updated: ' . $value->feedurl);
        }
        $this->info('Finished.');
    }

    public function updateFeed($xml, $feedid) {
        foreach ($xml->channel->item as $item) {
            $pubDate = Carbon::parse($item->pubDate);
            $newfeed = new \App\Feeds;
            $newfeed->feedsurls_id = $feedid;
            $newfeed->title = $item->title;
            $newfeed->text = $item->description;
            $newfeed->feed_page = $item->link;
            $newfeed->feed_time = $pubDate->format('Y-m-d H:m:s');
            $newfeed->save();
        }
        return;
    }

}
