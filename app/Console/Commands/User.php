<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class User extends Command
{

    protected $signature = 'user:create';
    protected $description = 'Create new user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
    	$name = $this->ask('User name: ');
    	$email = $this->ask('User email: ');
    	$password = $this->ask('User password: ');
    	
    	DB::table('users')->insert(
    		['email' => $email, 'password' => bcrypt($password), 'name' => $name]
		);

        $this->info('inserted');
    }
}
