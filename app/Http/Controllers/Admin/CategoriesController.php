<?php

namespace App\Http\Controllers\Admin;

use App\Categorie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller {

    public function index($categoryid = 0) {
        $categoryform = Categorie::find($categoryid);
        $categories = Categorie::all();
        return view('admin/categories', ['categories' => $categories, 'categoryform' => $categoryform]);
    }

    public function crud(Request $request) {
        switch ($request->input('action')) {
            case 'update':
                $this->update($request->input());
                break;
            case 'create':
                $this->create($request->input());
                break;
            case 'delete':
                $this->delete($request->input());
                break;
        }
        return redirect('/admin/categories');
    }

    public function create($request) {
        $category = new Categorie;
        $category->name = $request["categoryname"];
        $category->save();
        return redirect('/admin/categories');
    }

    public function delete($request) {
        $category = Categorie::find($request["id"]);
        $category->delete();
        return redirect('/admin/categories');
    }

    public function update($request) {
        // dd($request);
        $category = Categorie::find($request["id"]);
        $category->name = $request["categoryname"];
        $category->save();
        return redirect('/admin/categories');
    }

}
