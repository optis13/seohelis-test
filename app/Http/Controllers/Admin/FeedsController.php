<?php

namespace App\Http\Controllers\Admin;

use App\Feedsurls;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedsController extends Controller {

    public function index($feedid = 0) {
        $feedsurls = Feedsurls::all();
        $feedform = Feedsurls::find($feedid);
        $items = \App\Categorie::orderBy('name')->pluck('name', 'id');

        return view('admin/feeds', ['feedsurls' => $feedsurls, 'feedform' => $feedform, 'items' => $items]);
    }

    public function crud(Request $request) {
        switch ($request->input('action')) {
            case 'update':
                $this->update($request->input());
                break;
            case 'create':
                $this->create($request->input());
                break;
            case 'delete':
                $this->delete($request->input());
                break;
        }
        return redirect('/admin/feeds');
    }

    public function create($request) {
        $feed = new Feedsurls;
        $feed->feedurl = $request["feedurl"];
        $feed->category_id = $request["category_id"];
        $feed->feed_provider = $request["feed_provider"];
        $feed->save();
        return redirect('/admin/feeds');
    }

    public function delete($request) {
        $feed = Feedsurls::find($request["id"]);
        $feed->delete();
        return redirect('/admin/feeds');
    }

    public function update($request) {
        $feed = Feedsurls::find($request["id"]);
        $feed->feedurl = $request["feedurl"];
        $feed->category_id = $request["category_id"];
        $feed->feed_provider = $request["feed_provider"];
        $feed->save();
        return redirect('/admin/feeds');
    }

}
