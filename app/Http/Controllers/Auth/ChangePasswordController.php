<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ChangePasswordController extends Controller {

    public function index() {
        return view('auth/changepassword', ['error' => '']);
    }

    public function changepassword(Request $request) {
        if ($request->input('newpassword') == $request->input('confirmpassword')) {
            $user = \App\User::find(\Auth::id());
            $user->password = bcrypt($request->input('newpassword'));
            $user->save();
            return redirect('/feeds');
        }
        return view('auth/changepassword', ['error' => 'Password not cahnged. Password mismatch']);
    }

}
