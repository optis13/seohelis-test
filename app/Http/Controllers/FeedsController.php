<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Categorie;
use App\Feeds;
use App\Feedsurls;

class FeedsController extends Controller {

    private $categorieid;

    public function index() {
        $categories = Categorie::all();
        $feeds = DB::table('feeds')->join('feedsurls', function ($join) {
                    $join->on('feeds.feedsurls_id', '=', 'feedsurls.id');
                })->select('feeds.*', 'feedsurls.feed_provider')->orderBy('feed_time', 'desc')->get();
        return view('feeds', ['categories' => $categories, 'feeds' => $feeds]);
    }

    public function feeds($categorieid = 0) {
        $this->categorieid = $categorieid;
        $categories = Categorie::all();
        if ($categorieid > 0) {
            $feeds = DB::table('feeds')->join('feedsurls', function ($join) {
                        $join->on('feeds.feedsurls_id', '=', 'feedsurls.id')
                                ->where('feedsurls.category_id', '=', $this->categorieid);
                    })->select('feeds.*', 'feedsurls.feed_provider')->orderBy('feed_time', 'desc')->get();
        } else {
            $feeds = DB::table('feeds')->join('feedsurls', function ($join) {
                        $join->on('feeds.feedsurls_id', '=', 'feedsurls.id');
                    })->select('feeds.*', 'feedsurls.feed_provider')->orderBy('feed_time', 'desc')->get();
        }

        return view('feeds', ['categories' => $categories, 'feeds' => $feeds]);
    }

}
