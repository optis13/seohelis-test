
## Task for https://bitbucket.org/Sauls/seohelis-devdocs/wiki/new-team-member/backend/task2.md ##

### 1. MySQL DB ###
* Create mysql DB 'seohelistest'
* Import file seohelistest.sql into DB.
* Setup DB_CONNECTION=mysql in .env and config/database.php

### 2. Change 'url' in config/app.php ###

### 3. Create admin user using command line 'php artisan user:create' ###

### 4. In browser go to your 'url' address. ###
* Login with your user.
* Admin categories and feed urls.

### 5. Update feeds using command line 'php artisan feed:update' ###