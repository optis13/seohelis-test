@extends('layouts.app')

<script>

    function openModal(feedid) {
    $("#feedmodal").find(".modal-title").html($("#feedid_" + feedid + " .feedtitle").html());
    $("#feedmodal").find(".modal-body").html($("#feedid_" + feedid + " .feedtext").html());
    $('#feedmodal').modal({backdrop: 'static', keyboard: false})
            .on('click', '#goto_feedpage', function (e) {
            var url = $("#feedid_" + feedid + " .feedpage").html();
            window.open(url, '_blank');
            });
    }

    function filterFeed(categoryid) {
    location.href = "<?php echo str_replace(Request::path(), "", Request::url()); ?>" + "feeds/" + categoryid;
    }

    function destroyModalClick() {
    $('#feedmodal').modal({backdrop: 'static', keyboard: false}).off('click', '#goto_feedpage');
    }
</script>

@section('content')
<div class="flex-center position-ref full-height">

    <div class="content">

        <div class="row">
            <div class="col-md-1">

            </div>
            <div class="col-md-2">
                @foreach ($categories as $category)
                @if ($category['id'] == Request::segment(2))
                <div class="categoryactive" onclick="filterFeed({{ $category->id }});">{{ $category->name }}</div>
                @else
                <div class="category" onclick="filterFeed({{ $category->id }});">{{ $category->name }}</div>
                @endif
                @endforeach
            </div>
            <div class="col-md-8">
                @foreach ($feeds as $feed)
                <div class="feedblock" id="feedid_{{ $feed->id }}">
                    <div class="feedpage notdisplay">{{ $feed->feed_page }}</div>
                    <div class="feedtitle" onclick="openModal({{ $feed->id }});">{{ $feed->title }}</div>
                    <div class="feedtext">({{ $feed->feed_time }}) <a href="{{ $feed->feed_page }}" target="_blank">{{ $feed->feed_provider }}</a><br> {{!! $feed->text !!}}</div>
                </div>
                @endforeach
            </div>
            <div class="col-md-1">

            </div>
        </div>
    </div>
</div>


<div class="modal" id="feedmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button onclick="destroyModalClick();" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Header</h4>
            </div>
            <div class="modal-body">
                <p>text</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" id="goto_feedpage">Go to feed page</button>
                <button onclick="destroyModalClick();" type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection