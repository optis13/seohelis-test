@extends('layouts.app')

<script>

    function editfeed(categoryid) {
    location.href = "<?php echo str_replace(Request::path(), "", Request::url()); ?>" + "admin/categories/" + categoryid;
    }

    function changeaction(action) {
    $("#action").val(action);
    $("#formcategories").submit();
    }

</script>

@section('content')
<div class="flex-center position-ref full-height">

    <div class="content">

        <div class="row">
            <div class="col-md-1">

            </div>
            <div class="col-md-2">
                @foreach ($categories as $category)
                <div class="category" onclick="editfeed({{ $category->id }});">{{ $category->id }} - {{ $category->name }}</div>
                @endforeach
            </div>
            <div class="col-md-8">
                {!! Form::open(array('method' => 'POST', 'url' => 'admin/categories', 'id'=>'formcategories')) !!}

                <div class="form-group">
                    <div class="col-md-6">
                        <input id="action" type="hidden" name="action" value="">
                        <input id="id" type="hidden" name="id" value="{{ $categoryform['id'] }}">
                        Name: <br><input id="categoryname" class="form-control" name="categoryname" value="{{ $categoryform['name'] }}" required autofocus>
                        <div class="feedtext"><br></div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">

                        @if ($categoryform['id'] == 0)
                        <button onclick="changeaction('create');" type="button" class="btn btn-primary">
                            Insert
                        </button>
                        @endif

                        @if ($categoryform['id'] > 0)
                        <button onclick="changeaction('update');" type="button" class="btn btn-primary">
                            Update
                        </button>
                        @endif

                        @if ($categoryform['id'] > 0)
                        <button onclick="changeaction('delete');" type="button" class="btn btn-primary">
                            Delete
                        </button>
                        @endif

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-md-1">

            </div>
        </div>
    </div>
</div>


<div class="modal" id="feedmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Header</h4>
            </div>
            <div class="modal-body">
                <p>text</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" id="goto_feedpage">Go to feed page</button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection